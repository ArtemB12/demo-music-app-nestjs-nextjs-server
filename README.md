# Demo Music App - Server

## Requirements:

For run you need: [Client](https://gitlab.com/ArtemB12/demo-music-app-nestjs-nextjs-client)

## For run:

2) Start server:

```
npm i
npm run start:dev
```

1) Then run your client (go to readme.md and follow instructions)
